package org.coderkitchen.kitchens.springrestkitchen;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.annotation.PostConstruct;
import javax.xml.ws.Response;
import java.util.HashMap;
import java.util.List;
import java.util.Optional;

/**
 * Created by kunal_patel on 08/08/17.
 */
@RestController
@RequestMapping(value = "/ingrediants")
public class IngrediantsController {

    private static final Logger LOGGER = LoggerFactory.getLogger(IngrediantsController.class);

    private static final HashMap<Integer, List<String>> INMEMORY_INGREDIANTES = Maps.newHashMap();
    private static final int DEFAULT_INGREDIANT_ID = 1;

    @RequestMapping(value = {"", "/{id}"}, method = RequestMethod.GET)
    public ResponseEntity get(@PathVariable(value = "id") final Optional<Integer> id) {
        LOGGER.info("{}", id);
        final Integer key = id.isPresent() ? id.get() : DEFAULT_INGREDIANT_ID;

        return ResponseEntity.ok(INMEMORY_INGREDIANTES.get(key));
    }

    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity post(@RequestBody final IngrediantRequest request) {

    }

    @PostConstruct
    public void init() {
        INMEMORY_INGREDIANTES.put(1, Lists.newArrayList("OIL", "GHEE"));
        INMEMORY_INGREDIANTES.put(2, Lists.newArrayList("RICE", "WHEAT"));
        INMEMORY_INGREDIANTES.put(3, Lists.newArrayList("MILK", "CURD", "BUTTER"));
    }

}
